package com.neusoft;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @ClassName Cocalculator
 * @Description //TODO
 * @Author mengxianquan
 * @Date 2021/6/29 21:24
 * @Version 1.0
 **/
public class Cocalculator {
    public static void main(String[] args) throws Exception {
        ServerSocket server = new ServerSocket(8080);
        while (true) {
            Socket socket = server.accept();
            InputStreamReader r = new InputStreamReader(socket.getInputStream());
            BufferedReader br = new BufferedReader(r);
            String readLine = br.readLine();
            Integer result = null;
            String html = "http/1.1 200 ok\n"
                    + "\n\n";
            if (readLine.indexOf("?") > 0) {

                String counter = readLine.substring(readLine.indexOf("/") + 1, readLine.indexOf("?"));
                Integer a = Integer.parseInt(readLine.substring(readLine.indexOf("=") + 1, readLine.indexOf("&")));
                Integer b = Integer.parseInt(readLine.substring(readLine.lastIndexOf("=") + 1, readLine.lastIndexOf(" ")));

                if ("add".equals(counter)) {
                    result = a + b;
                } else if ("mult".equals(counter)) {
                    result = a * b;
                }

            }
            if (result==null){
                html += "Incorrect domain name input";
            }else {
                html += result;
            }

            PrintWriter pw = new PrintWriter(socket.getOutputStream());
            pw.println(html);
            pw.close();
        }
    }
}

